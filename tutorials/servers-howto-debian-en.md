# Minetest Server

**Minetest package** from Debian although it is played on your computer or phone, it can also be played online, 
that means it is a client - server game, **where the client is the guest and the server is the host.**

## Knowning server and client

Minetest (also multicraft too as same) in Debian are separated in two packages, the server and the player, 
the server is renamed to `minetest-server`, and client/player is named `minetest`, there's another package 
for the common files like fonts etc named `minetest-data` that must be need on each case.

**The server part (inclusivelly when you are using the `minetest` normal program to play, which embeds a server already)
provides a remote host of the game, so client just use all resources from network.**

## Running the Server

The minetest package provides a daemon service named `minetest-server` running with the specific user `Debian-minetest`.

The minetest package until Debian 10 is 0.4 releases, since debian 11 minetest is 5.X releases, in `buster` we 
can run 5.0 throught backports packages, in `stretch` we can run 0.4.17 throught backports packages.

## Choosing server

Mostly all tutorials recommends use a VPS or dedicated server to host a game server, but this is not necesary.. 
**the real requirements are the bandwidth and the stability of the operating system**. (By example residential Internet 
connections tend to be unreliable and also have less upload speed, or not be able to keep a server online 24/7).

So then those are the point to fill when plans to deploy a public server:

* **Networking**: you must have a decent bandwidth of **upload speed** (that is not same as download one)
* **System**: you must use a simple OS like alpine focused on **stability** (that is not the same of fashioned)
* **Storage**: is recommended to use SSD or faster RAID disks due **I/O performance** (mods uses many plain files)

Rest of requirements are most fine, by example 1G Ram if the server is dedicated to minetest are so enough, but 
over debian if yu plan to host many heavy mods will need 8G RAM recommended.

## General Deploy

The minetest server uses some artifact that you must take in care before start, but first of all:

1. We will assume you already downloaded the server software or compiled yourselft.
2. We will created a dedicated user to run the server, no cares if there's already one similar to.
3. We will assume a `xdg` debian compilant version, that means all will be integrated in specific paths
4. We will assume that you are using only Debian oficial packages.

#### Create a dedicated user if not exist

This step is already done in Debian systems, A Debian-minetest system user is set up during the package 
installation process, which is used to run minetest servers. 

* The minetest user is `Debian-minetest`
* User home is in `/var/games/minetest-server`
* System mods and buildin ones are taken from `/usr/share/games/minetest`

The Debian `minetest-server` package comes with "shitstemd" unit files, it features as a template format 
if you plan to run several servers on the same host. Lest explain that later. Now with a dedicated user we 
must know the artifact parts of the minetest server to deploy.

#### Minetest server parts artifacts

A minetest server need as minimun of 3 parameters to indicate: game id, config file and world path

* **gameid** : a game itself is a directory with collection of mods that defines behaviour of the game, those 
will load the files inside mods directories, **Debian package assume** `xdg` **so gameid are located at** 
`/var/games/minetest-server/.minetest/games`, there are some famous:
    * `minetest_game` is the default one.. download from https://github.com/minetest/minetest_game/tags 
    and must match the minetest version runing, is the most stable and basic to run.
    * `extreme_survival` is a huge all in one but very dificult, it must be downloaded 
    from https://github.com/maikerumine/ESM4/releases/tag/MT_0.4.16_FREEZE for 0.4 
    or https://github.com/maikerumine/ESM5/releases/tag/MT_0.5.0_Server for 5.2
    * `mineclone2` famous high featured full of things experience, it must be downloaded 
    from https://git.minetest.land/MineClone2/MineClone2/tags but there's only one version 
    for 0.4.X at https://github.com/davedevils/MineClone, since v0.51 are only for 5.X releases.
    * `clonecrat` like the previous one but lighter, download at https://github.com/notanewbie/clonecraft_ZIP 
    but is a older development so use only for 0.4 releases
* **world** : a world itself, is a directory with collection of files for environment and/or building(s) and 
all saved data associated with a world, **Debian package assume** `xdg` **so gameid are located at** 
`/var/games/minetest-server/.minetest/worlds`, by default, it (will) contains:
    * `auth.txt` (`auth.db`) is the file for players connected, with passwords and permissions, 
    the `auth_rx` manages as "db" in similar way, but added two extra columns, login dates and time alive.
    * `env_meta.txt` it contains the values specific for this world, of configuration by example 
    for map generation, time passed sinde begin of the world, time of the day, etc.
    * `map_meta.txt` it contains the configuration of imple global map variables. The map generator 
    can be defined here and tweaked with parameters.
    * `map.sqlite` (`map.txt`) is the content of the mad data of echa node, block or stuff in the game, 
    related to core engine and map generation.
    * `world.mt` is the definition of the world, is the only file that must be present before anything, 
    if the world is new or if the other files are already present, this must be in sync with.
* **minetest.conf** : the config file that the server must use to start, it defines the path to the world place 
and the usage of the gameid of the world, also paramteters to run the host of the game, **This file can 
be used in Debian in conjunction with shitstemd template feature, so specifid server instances are defined by** 
`/etc/minetest/` **directory and each config file**, each config file need settings but the minimal ones 
to be configure are:
    * `map-dir` it defines the `world` to be used by direct path of the world directory.
    * `default_game` it defines the `gameid` to be used inside the running minetest server adn for the world.

The `map-dir` is only need if you plan to use the shitstemd template feature, if you will run only one server, 
this parameter of the config file can be commented or just use default path. But if you have more that one 
world you must specify this config setting.

Rest of files and setting are enough as default and must be tuned depending of the target of the dessired game.

#### Minetest server running the parts

Once defined the artifact parts of the server to deploy, we must invoke the software, based on the previous 
minimal artifact parts those are the minimal parameters to use:

1. Login as the user of the running server `su -l Debian-minetest`
2. create or edit the config file in `/etc/minetest/minetest.conf` the default one is enought
3. run the service as `/usr/sbin/service minetest-server restart`

If your server crashes, then look at the `minetest.log` in `/var/log/minetest/` cos lest remenber that 
we are using a `xdg` pacakge integrated to system.

#### Networking bus nets

If you are usin some kind of VPS or are inside a private network (like behind of a router), players outside 
of your network won't be able to join the server unless you port forward.

* Choose a port to run the server on. 
    * The default of 30000 can be used, but can be changed in `port` of `minetest.conf` file
* Find your ip adddress internally, by example just type `ip addr`
* Login to your router, and port forward UDP on your chosen port to the internal IP you found
* Alter any firewalls you may have to allow traffic to the port you choose
* Make your server listed in the server list by setting the following settings in `minetest.conf`
     * change `server_address` to the public ip of the router (yes must be changed if you  provider change it)
* restart the server and network

## Advanced deploy

When setting up a new server, you should consider which protections are needed. This is extremely 
important for public servers, because you cannot predict who will connect or what they will do 
on your server.

#### Common problems include:

* Accidental or intentional damage to other players' work (griefing).
* Chat spam (may include swearing or advertisements).
* Aggression, harassment or other unwanted behaviours between players.
* Trouble makers who evade bans.
* Impersonation of well known people within the MT community.
* Bugs, for example caused by mods, which allow a malicious player to execute arbitrary system commands.

Note that tnt and fire spreading, which are both enabled by default in singleplayer, 
are disabled by default on servers.

#### Managing your server

TODO

##### Using template shitstemd

You can use a systemd template feature to run several instances of server with same package in same host.

* Must have a especific config file in `/etc/minetest` by example `/etc/minetest/serverskyworld.conf`
* Must run the daemon with `@` by example for config file below will be `/usr/sbin/service minetest@serverskyworld restart`

You can find a commented server configuration file on `/usr/share/doc/minetest/minetest.conf.example.gz` 
that can be used as template for start to editiing with command: 
`zcat /usr/share/doc/minetest/minetest.conf.example.gz > /etc/minetest/serverskyworld.conf`

#### Especific guides for

* Debian server guide for default debian packages
* Debian server guides for VenenuX powered packages
* Centos/fedora server guide for stupid windosers
* Alpine server guide for high pro users and hackers
