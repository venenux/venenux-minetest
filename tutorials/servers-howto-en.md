# Minetest Server

**Minetest is a game** that although it is played on your computer or phone, it can also be played online, 

Means it is a client - server game, **where the client is your phone/pc** and the server can be your phone/pc or a remote internet computer.

## Knowning server and client

Minetest (also multicraft too as same) is a program that can acts as server and client at same time, 
but there's a special compilation/binary that only acts as server, normally called `minetestserver`,
(some distributions renamed to `minetest-server` like those based on Debian)

**The server part (inclusivelly when you are using the `minetest` normal program to play, which embeds a server already)
provides a remote host of the game, so client just use all resources from network.**

## Running the Server

Mostly safe tutorial recommends blurry OS, but if you are a windoser, sorry, a solid rock server must be focused, 
but if you have no choices, then you always run the server as a dedicated user.

A dedicated user is always recommended and used, most distributions uses a username named `minetest`, 
(some distributions renamed as `Debian-minetest` like those based on Debian)

## Choosing server

Mostly all tutorials recommends use a VPS or dedicated server to host a game server, but this is not necesary.. 
**the real requirements are the bandwidth and the stability of the operating system**. (By example residential Internet 
connections tend to be unreliable and also have less upload speed, or not be able to keep a server online 24/7).

So then those are the point to fill when plans to deploy a public server:

* **Networking**: you must have a decent bandwidth of **upload speed** (that is not same as download one)
* **System**: you must use a simple OS like alpine focused on **stability** (that is not the same of fashioned)
* **Storage**: is recommended to use SSD or faster RAID disks due **I/O performance** (mods uses many plain files)

Rest of requirements are most fine, by example 1G Ram if the server is dedicated to minetest are so enough, only 
if you are using the recommended operating system like Alpine by example, in other cases 4G RAM is recommended.

## General Deploy

The minetest server uses some artifact that you must take in care before start, but first of all:

1. We will assume you already downloaded the server software or compiled yourselft.
2. We will created a dedicated user to run the server, no cares if there's already one similar to.
3. We will assume a `RUN_IN_PLACE` version, that means all will be take from running path
4. We will assume all the hole program localed at `/usr/local/minetest`

#### Create a dedicated user if not exist

This step is already done in some distributions. But if you compiled or setup your `minetestserver` then 
you must create your own user to run:

```
adduser -D -d /usr/local/minetest minetestsrv

 echo "minetestsrv:kaypassword2123" | chpasswd

chown -R minetestsrv:minetestsrv /usr/local/minetest
```

Note the usage of extra spaces in the password asignation. Now with a dedicated user we must know the 
artifact parts of the minetest server to deploy.

#### Minetest server parts artifacts

A minetest server need as minimun of 3 parameters to indicate: game id, config file and world path

* **gameid** : a game itself is a directory with collection of mods that defines behaviour of the game, those 
will load the files inside mods directories, **Due we are assuming** `RUN_IN_PLACE` **they are located at** 
`/usr/local/minetest/games`, but if you are running as system wide, are located at `$HOME/.minetest/games`; **you must download under the games path (generally as a unique directory)**, 
there are some famous:
    * `minetest_game` is the default one.. download from https://github.com/minetest/minetest_game/tags 
    and must match the minetest version runing, is the most stable and basic to run.
    * `extreme_survival` is a huge all in one but very dificult, it must be downloaded 
    from https://github.com/maikerumine/ESM4/releases/tag/MT_0.4.16_FREEZE for 0.4 
    or https://github.com/maikerumine/ESM5/releases/tag/MT_0.5.0_Server for 5.2
    * `mineclone2` famous high featured full of things experience, it must be downloaded 
    from https://git.minetest.land/MineClone2/MineClone2/tags but there's only one version 
    for 0.4.X at https://github.com/davedevils/MineClone, since v0.51 are only for 5.X releases.
    * `clonecrat` like the previous one but lighter, download at https://github.com/notanewbie/clonecraft_ZIP 
    but is a older development so use only for 0.4 releases
* **game.conf** : the config file that the server try to load over minetest.conf when the `game_id` loads.
    * Loading path: it loads from the directory of the game that are currently loading.
    * Contains paramteters to run the host:
         * `title`: (Required), the name of the directory where game parts will be loaded
         * `name`: (Deprecated) same as title, used for minetst from 0.X to 5.4 releases.
         * `description`: Short description to be shown in the content tab, no more than 128 chars
         * `allowed_mapgens = <comma-separated mapgens>` e.g `v5,v6,flat`, Mapgens not in this 
         list are removed from the list of mapgens for the game. If not specified, all mapgens are allowed.
         * `disallowed_mapgens = <comma-separated mapgens>` e.g. `v6,flat` These mapgens are removed from the 
         list of mapgens for the game, allowed_mapgens is applied before disallowed_mapgens.
         * `disabled_settings = <comma-separated settings>` e.g. `disabled_settings = enable_damage, creative_mode` 
         these settings are hidden for this game in the "Start game" tab and will be initialized as false when 
         the game is started. Prepend a setting name with an exclamation mark to initialize it to true (this does 
         not work for enable_server). Only these settings are supported: `enable_damage, creative_mode, enable_server`.
         * `map_persistent`: Specifies whether newly created worlds should use a persistent map backend. 
         Defaults to true (= "sqlite3")
         * `author`: The author of the game. It only appears when downloaded from ContentDB.
* **world** : a world itself, is a directory with collection of files for environment and/or building(s) and 
all saved data associated with a world, **Due we are assuming** `RUN_IN_PLACE` **they are located at** 
`/usr/local/minetest/worlds`, **and if not pointed in config will be loaded one named `world`** 
by default, it (will) contains:
    * `auth.txt` (`auth.db`) is the file for players connected, with passwords and permissions, 
    the `auth_rx` manages as "db" in similar way, but added two extra columns, login dates and time alive.
    * `env_meta.txt` it contains the values specific for this world, of configuration by example 
    for map generation, time passed sinde begin of the world, time of the day, etc.
    * `map_meta.txt` it contains the configuration of imple global map variables. The map generator 
    can be defined here and tweaked with parameters.
    * `map.sqlite` (`map.txt`) is the content of the mad data of echa node, block or stuff in the game, 
    related to core engine and map generation.
    * `world.mt` is the definition of the world, is the only file that must be present before anything, 
    if the world is new or if the other files are already present, this must be in sync with.
* **minetest.conf** : the config file that the server must use to start, it defines the path to the world place 
and the usage of the gameid of the world, you will now rarely need to edit minetest.conf manually cos the GUI 
screen provide you a interface to confgure it all. This file is read every time the game starts and is always 
created/modified when the menu quits.
    * Loading path: it loads from:
         * `/usr/local/minetest/games/<gameid>/minetest.conf` if you have system wide with `/usr/local` as prefix
         and build the game with `RUN_IN_PLACE=0` (means it loads from the gameid directory of the game first)
         * `$HOME/.minetest/games/<gameid>/minetest.conf` as second try if the game is running from user wide
         and build the game with `RUN_IN_PLACE=0` (means it loads from the gameid user directory then)
         * `../minetest.conf` if you are configuring build for `RUN_IN_PLACE=1` that means all the minetest 
         files are running from one place.
         * `../../minetest.conf` also, but will not write to it, if you are configuring build for `RUN_IN_PLACE=1` 
         that means all the minetest files are running from one place.
         * `~/.minetest/minetest.conf` if you are running a system wide installation, like a package 
         from distribution or if configuring build for `RUN_IN_PLACE=0`
         * Custom path in command line using the option `--config /path/to/minetest.conf` of the server 
         binary program.
    * Contains paramteters to run the host:
         * `default_game` it defines the `gameid` to be used inside the running minetest server adn for the world.

Rest of files and setting are enough as default and must be tuned depending of the target of the dessired game.

#### Minetest server running the parts

Once defined the artifact parts of the server to deploy, we must invoke the software, based on the previous 
minimal artifact parts those are the minimal parameters to use:

1. Login as the user of the running server `su -l minetestsrv`
2. type the command of the server to run based on artifacts:
    * executable: `/usr/local/minetest/bin/minetestserver`
    * use the tuned config file so added `--config /usr/local/minetest/minetest.conf`, put this file in bin directory
    * define the game id so added `--gameid minetest_game`
    * define the world name if there are more than one directory so added `--worldname world`
    * define a log file to dump anything so added `--logfile debug.txt --info`
3. So the complete minimal command line will be 
    * `/usr/local/minetest/bin/minetestserver --config /usr/local/minetest/minetest.conf --gameid minetest_game --worldname world --logfile debug.txt --info`

If your server crashes, then look at the `debug.txt` in `/usr/local/minetest/bin/` cos lest remenber that 
we are using a `RUN_IN_PLACE` binary downloaded from. On some distributions the log file are dumped 
inside the syslog activity.

#### Networking bus nets

If you are usin some kind of VPS or are inside a private network (like behind of a router), players outside 
of your network won't be able to join the server unless you port forward.

* Choose a port to run the server on. 
    * The default of 30000 can be used, but can be changed in `port` of `minetest.conf` file
* Find your ip adddress internally, by example just type `ip addr`
* Login to your router, and port forward UDP on your chosen port to the internal IP you found
* Alter any firewalls you may have to allow traffic to the port you choose
* Make your server listed in the server list by setting the following settings in `minetest.conf`
     * change `server_address` to the public ip of the router (yes must be changed if you  provider change it)
* restart the server and network

## Advanced deploy

When setting up a new server, you should consider which protections are needed. This is extremely important for public servers, because you cannot predict who will connect or what they will do on your server.

#### Common problems include:

* Accidental or intentional damage to other players' work (griefing).
* Chat spam (may include swearing or advertisements).
* Aggression, harassment or other unwanted behaviours between players.
* Trouble makers who evade bans.
* Impersonation of well known people within the MT community.
* Bugs, for example caused by mods, which allow a malicious player to execute arbitrary system commands.

Note that tnt and fire spreading, which are both enabled by default in singleplayer, are disabled by default on servers.

#### Managing your server

TODO

#### Especific guides for

* Debian server guide for default debian packages
* Debian server guides for VenenuX powered packages
* Centos/fedora server guide for stupid windosers
* Alpine server guide for high pro users and hackers
